
<?php

// Afficher les erreurs à l'écran
ini_set('display_errors', 1);
// Enregistrer les erreurs dans un fichier de log
ini_set('log_errors', 1);
// Nom du fichier qui enregistre les logs (attention aux droits à l'écriture)
ini_set('error_log', dirname(__file__) . '/log_error_php.txt');
// Afficher les erreurs et les avertissements
//error_reporting(e_all);

echo 'Page de production de message', "<br />";

$dossier = 'Upload/';
$fichier = basename($_FILES['fileToUpload']['name']);
$taille_maxi = 10000000;
$taille = filesize($_FILES['fileToUpload']['tmp_name']);
$extensions = array('.png', '.gif', '.jpg', '.jpeg','.pdf','.txt','.avi');
$extension = strrchr($_FILES['fileToUpload']['name'], '.');

//Début des vérifications de sécurité...
if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
{
     $erreur = 'Vous devez uploader un fichier de type png, gif, jpg, jpeg, txt ou doc...';
}
if($taille>$taille_maxi)
{
     $erreur = 'Le fichier est trop gros...';
}
//if (file_exists("$dossier.$fichier)) {
//    $erreur = 'Vous devez choisir un autre nom de fichier';
//}

//S'il n'y a pas d'erreur, on upload !
if(!isset($erreur))
{
     //On formate le nom du fichier ici...
     $fichier = strtr($fichier, 
          'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 
          'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
     $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);
     if(move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
     {
	echo 'Upload effectué avec succès !';
	$hash = hash_file('sha256', $dossier.$fichier);

	$rk = new RdKafka\Producer();
	$rk->setLogLevel(LOG_DEBUG);
	$rk->addBrokers("127.0.0.1");

	$topic = $rk->newTopic("testLionel");

	$topic->produce(RD_KAFKA_PARTITION_UA, 0, $hash);
	$topic->produce(RD_KAFKA_PARTITION_UA, 0, htmlspecialchars($_POST['URL']));

	echo "<br />", 'Le message a été envoyé', "<br />";

	?>

	Le hash envoyé est le suivant : <?php echo $hash, "<br>"; ?>
	Le lien URL du fichier envoyé est le suivant : <?php echo htmlspecialchars($_POST['URL']); 

     }
     else //Sinon (la fonction renvoie FALSE).
     {
          echo 'Echec de l\'upload !';
     }
}
else
{
     echo $erreur;
}

?>