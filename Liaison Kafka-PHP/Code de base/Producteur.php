<?php

echo 'Page de production de message', "<br />";

$rk = new RdKafka\Producer();
$rk->setLogLevel(LOG_DEBUG);
$rk->addBrokers("127.0.0.1");

$topic = $rk->newTopic("testLionel");

$topic->produce(RD_KAFKA_PARTITION_UA, 0, "Lionel !");

echo "<br />", 'Le message a été envoyé', "<br />";

?>