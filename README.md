**TRUEACK**
===========

Cette application est un equivalent à l'accusé réception de Laposte en numérique.  
L'utilisateur peut envoyer un message avec preuve d'envoi et recevoir une preuve de lecture du message envoyé.  

La certifications de l'envoi et de la lecture se font grâce à la blockchain.  


