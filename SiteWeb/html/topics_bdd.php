	<?php

			/*##################################################
			#	   topics.bdd.php                          #
			#       Ver 1.0 - Date 13-12-2018                  #
			#       remplissage de la bdd pour les topics      #
			##################################################*/			


			// Afficher les erreurs à l'écran
			// ini_set('display_errors', 1);

			// Elements de connexion à la BDD
			require_once ("paramcon.php");

			// On se connecte à la base de données
			$connexion = new PDO('mysql:host='.$lehost.';port='.$leport.';dbname='.$dbname, $user, $pass);

			// On envoie la requête pour remplir la base
			$req = $connexion->prepare('INSERT INTO topics(topics, password) VALUES(:topics, :password)');
			$req->execute(array(
				'topics' => $nom_topic,
				'password' => password_hash($passwd, PASSWORD_DEFAULT)
				));

	?>
