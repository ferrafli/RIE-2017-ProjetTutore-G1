<?php
/*##################################################
#	    mail_certif.php                            #
#       Ver 1.0 - Date 09-12-2018                  #
#       Liaison avec certifiant                    #
##################################################*/

	// Afficher les erreurs à l'écran
	//ini_set('display_errors', 1);

	// Déclaration de l'adresse de destination.
	$mail = $_SESSION['email'];
	
	// On filtre les serveurs qui rencontrent des bogues.
	if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) 
	{
		$passage_ligne = "\r\n";
	}
	else
	{
		$passage_ligne = "\n";
	}
	
	//=====Déclaration des messages au format texte et au format HTML.
	$message_txt = utf8_decode("Salut à tous, voici un e-mail envoyé par un script PHP.");
	$message_html = utf8_decode("<html>
									<head></head>
									<body>
										<b>Bonjour à vous,</b><br><br>
										<p>Nous vous confirmons que votre courrier a bien été envoyé par l'équipe de TRUEACK.</p><br>
										<p>Il s'agit d'un courrier de type accusé reception.</p><br>
										<p>Votre topic porte le nom suivant : $nom_topic</p><br>
										<p> voici votre preuve d'envoi : $preuve_env .</p><br>

									</body>
								</html>");
	//==========
	 
	//=====Création de la boundary
	$boundary = "-----=".md5(rand());
	//==========
	 
	//=====Définition du sujet.
	$sujet = utf8_decode("Envoi de votre AR electronique !");
	//=========
	 
	//=====Création du header de l'e-mail.
	$header = "From: \"TrueAck\"<message@trueack.ovh>".$passage_ligne;
	$header.= "Reply-to: \"TrueAck\" <message@trueack.ovh>".$passage_ligne;
	$header.= "MIME-Version: 1.0".$passage_ligne;
	$header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
	//==========
	 
	//=====Création du message.
	$message = $passage_ligne."--".$boundary.$passage_ligne;
	//=====Ajout du message au format texte.
	$message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
	$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
	$message.= $passage_ligne.$message_txt.$passage_ligne;
	//==========
	$message.= $passage_ligne."--".$boundary.$passage_ligne;
	//=====Ajout du message au format HTML
	$message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
	$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
	$message.= $passage_ligne.$message_html.$passage_ligne;
	//==========
	$message.= $passage_ligne."--".$boundary."--".$passage_ligne;
	$message.= $passage_ligne."--".$boundary."--".$passage_ligne;
	//==========
	 
	//=====Envoi de l'e-mail.
	mail($mail,$sujet,$message,$header);
	//==========
?>
