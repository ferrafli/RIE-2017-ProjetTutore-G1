<?php
/*##################################################
#	    producteur.php                             #
#       Ver 1.3 - Date 09-12-2018                  #
#       produire le message du client              #
##################################################*/

// Afficher les erreurs à l'écran
// ini_set('display_errors', 1);

// Démarrage ou restauration de la session
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" type="image/x-icon" href="/Imgs/favicon.ico" />


	<title>Trueack</title>

	<!-- Bootstrap core CSS -->
	<link href="./style/css/bootstrap.css" rel="stylesheet">

	<!-- Custom styles for this template -->
<link href="./style/css/sticky-footer-navbar.css" rel="stylesheet">
</head>

	<body>
		<?php include_once('header.php'); ?>
		<?php include('footer.php'); ?>
              	<h1>Page de production de message<br></h1>

		<?php

		//Fonction de génération d'un mot de passe pour l'accés au topic
		function genpass($longueur = 6) {
			$caracteres = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$longueurMax = strlen($caracteres);
			$chaineAleatoire = '';
			for ($i = 0; $i < $longueur; $i++) {
				$chaineAleatoire .= $caracteres[rand(0, $longueurMax - 1)];
			}
			return $chaineAleatoire;
		}

		// Création du nom de topic
		$nom_topic = $_SESSION['login']."_".time();

		// Création du password pour la lecture du topic
		$passwd = genpass();

		// Création du dossier pour déposer le fichier
		if ( mkdir('Upload/'.$nom_topic.'/', 0777) == FALSE ) {
			?>
				<form class='error'>
					<p>Erreur sur la création du dossier en interne.</p>
					<input type="button" value="Retour" onclick="history.go(-1)">
				</form>
			<?php
			exit;
		}
		// Configuration des droits sur le dossier
		chmod('Upload/'.$nom_topic, 0777);

		$dossier = 'Upload/'.$nom_topic.'/';
		$fichier = basename($_FILES['fileToUpload']['name']);
		$taille_maxi = 10000000;
		$taille = filesize($_FILES['fileToUpload']['tmp_name']);
		$extensions = array('.png', '.gif', '.jpg', '.jpeg','.pdf','.txt','.avi');
		$extension = strrchr($_FILES['fileToUpload']['name'], '.');

		//Début des vérifications de sécurité...
		if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
		{
			?>
				<form class='error'>
					<p>Vous devez uploader un fichier de type png, gif, jpg, jpeg, txt ou doc...</p>
					<button class="btn bnt-outline-secondary"><a href="write.php">Retour</a></button>
				</form>
			<?php
			exit;
		}
		elseif($taille>$taille_maxi)
		{
			?>
				<form class='error'>
					<p>Le fichier est trop gros...</p>
					<button class="btn bnt-outline-secondary"><a href="write.php">Retour</a></button>
				</form>
			<?php
			exit;
		}
		// On vérifie maintenant si le champ "COM" n'est pas vide
		elseif(empty($_POST['COM'])) {
			?>
			<form class='error'>
				<p>Le champ "Commentaire" est vide.</p>
				<button class="btn bnt-outline-secondary"><a href="write.php">Retour</a></button>
			</form>
			<?php
			exit;
		}
		// On vérifie maintenant si le champ "MAIL" n'est pas vide
		elseif(empty($_POST['MAIL'])) {
			?>
			<form class='error'>
				<p>Le champ "Adresse mail de votre correspondant" est vide.</p>
				<button class="btn bnt-outline-secondary"><a href="write.php">Retour</a></button>
			</form>
			<?php
			exit;
		}

		//S'il n'y a pas d'erreur, on upload !
		else {
			echo $dossier;
		     //On formate le nom du fichier ici...
		     $fichier = strtr($fichier,
			  'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
			  'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
		     $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);
		     if(move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
		     {
			echo 'Upload effectué avec succès !';
			$hash = hash_file('sha256', $dossier.$fichier);

			$rk = new RdKafka\Producer();
			$rk->setLogLevel(LOG_DEBUG);
			$rk->addBrokers("127.0.0.1");
			// Création du nom de topic
			$nom_topic = $_SESSION['login']."_".time();
			$topic = $rk->newTopic($nom_topic);

			// Envoi des messages dans Kafka
			// Balise DATA non visible par l'utilisateur
			$topic->produce(RD_KAFKA_PARTITION_UA, 0, "#DATA_INVISIBLES#");
			// Date d'envoi
			$topic->produce(RD_KAFKA_PARTITION_UA, 0, date('d/m/Y à H:i', strtotime('now +2 Hours')));
			// Nom de la personne qui envoi
			$topic->produce(RD_KAFKA_PARTITION_UA, 0, $_SESSION['nom']);
			// Prenom de la personne qui envoi
			$topic->produce(RD_KAFKA_PARTITION_UA, 0, $_SESSION['prenom']);
			// Mail de la personne qui envoi
			$topic->produce(RD_KAFKA_PARTITION_UA, 0, $_SESSION['email']);
			// Mail de la personne qui reçoit
			$topic->produce(RD_KAFKA_PARTITION_UA, 0, htmlspecialchars($_POST['MAIL']));
			// Nom du fichier
			$topic->produce(RD_KAFKA_PARTITION_UA, 0, htmlspecialchars($fichier));
			// Balise DATA visible par l'utilisateur
			$topic->produce(RD_KAFKA_PARTITION_UA, 0, "#DATA_VISIBLES#");
			// Hash du document
			$topic->produce(RD_KAFKA_PARTITION_UA, 0, $hash);
			// Url du document
			$topic->produce(RD_KAFKA_PARTITION_UA, 0, htmlspecialchars($_POST['COM']));

			require_once("certif-env.php");

			// Envoi du mail d'information d'un nouvel échange
			require_once ("mail.php");
			require_once ("mail_certif.php");
			echo "<br />", 'Le message a été envoyé', "<br />";

			require_once ("topics_bdd.php");

			?>

			Le hash envoyé est le suivant : <?php echo $hash, "<br>"; ?>
			Le commentaire du fichier envoyé est le suivant : <?php echo htmlspecialchars($_POST['COM']), "<br>"; ?>
			L'adresse mail de la personne à contacter : <?php echo htmlspecialchars($_POST['MAIL']), "<br>"; ?>
			Le topic se nomme : <?php echo $nom_topic;

		    }
		    else //Sinon (la fonction renvoie FALSE).
		    {
				?>
				<form class='error'>
					<p>Echec de l'upload !</p>
					<button class="btn bnt-outline-secondary"><a href="write.php">Retour</a></button>
				</form>
				<?php
				exit;
		    }
		}

		?>

		<form>
			<button class="btn bnt-outline-secondary"><a href="accueil.php">Retour</a></button>
		</form>

        </body>
</html>
