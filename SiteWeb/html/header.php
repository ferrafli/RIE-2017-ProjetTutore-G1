
<html>
  <div id="header">
    <header>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
          <a class="navbar-brand" href="#">TRUEACK</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
              <?php
                if(!empty($_SESSION['login'])){

                  echo'<li class="nav-item">
                        <a class="nav-link" href="accueil.php">Accueil<span class="sr-only">(current)</span></a>
                        </li>';
                }
                else{
                  echo'<li class="nav-item">
                      <a class="nav-link" href="index.php">Accueil<span class="sr-only">(current)</span></a>
                      </li>';
                }
              ?>  

              <?php
                if(isset($_SESSION['login'])){

                  echo'<li class="nav-item">
                        <a class="nav-link" href="moncompte.php">Mon Compte</a>
                        </li>';
                }
              ?>
              
              <li class="nav-item">
                <a class="nav-link" href="contact.php">Contact<span class="sr-only">(current)</span></a>
              </li>  
             
            </ul>
            <form class="form-inline mt-2 mt-md-0" action="deconnexion.php">
              
              <?php 
                  if(isset($_SESSION['login'])){

                  echo '<div>
                        
                        <button class="btn btn-outline-success" type="submit">Déconnexion</button>
                        
                        </div>';
                }
              ?>
            </form> 
          </div>
        </nav>
    </header>
  </div>
</html>
