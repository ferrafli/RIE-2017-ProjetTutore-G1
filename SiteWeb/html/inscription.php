<!DOCTYPE html>
<html>
	<head>
			<meta charset="utf-8" />
			<title>Inscription</title>
			
			<!-- Bootstrap core CSS -->
			<link href="./style/css/bootstrap.css" rel="stylesheet">

			<!-- Custom styles for this template -->
			<link href="./style/css/sticky-footer-navbar.css" rel="stylesheet">
	</head>

	<body>
		<?php
			// Afficher les erreurs à l'écran
			// ini_set('display_errors', 1);

			/*##################################################
			#	    inscription.php                            #
			#       Ver 2.0 - Date 14-12-2018                  #
			#       Page de gestion de l'inscription           #
			##################################################*/

			//Appel au header du site
			include('header.php');

			//Appel au header du site
			include('footer.php');
			
			// Elements de connexion à la BDD
			require_once ("paramcon.php");

			// Déclaration du compteur pour déterminer la réussite de nos requêtes SQL
			$compt=0;

			// Phase de tests des champs entré par l'utilisateur
			if( empty($_POST['login']) ) {
			   // on vérifie que le champ "Login" n'est pas vide
			   // empty vérifie à la fois si le champ est vide et si le champ existe belle et bien (is set)
				?>
				<form class='error'>
					<p>Le champ login est vide.</p>
					<button class="btn bnt-outline-secondary"><a href="formulaire.php">Retour</a></button>
				</form>
				<?php
				exit;
			}
			// On vérifie maintenant si le champ "prenom" n'est pas vide et existe bien
			elseif(empty($_POST['prenom'])) {
				?>
				<form class='error'>
					<p>Le champ prenom est vide.</p>
					<button class="btn bnt-outline-secondary"><a href="formulaire.php">Retour</a></button>
				</form>
				<?php
				exit;
			}
			// On vérifie maintenant si le champ "nom" n'est pas vide et existe bien
			elseif(empty($_POST['nom'])) {
				?>
				<form class='error'>
					<p>Le champ nom est vide.</p>
					<button class="btn bnt-outline-secondary"><a href="formulaire.php">Retour</a></button>
				</form>
				<?php
				exit;
			}
			// on vérifie maintenant si le champ "Password" n'est pas vide et existe bien
			elseif(empty($_POST['password'])) {
				?>
				<form class='error'>
					<p>Le champ mot de passe est vide.</p>
					<button class="btn bnt-outline-secondary"><a href="formulaire.php">Retour</a></button>
				</form>
				<?php
				exit;
			}
			// On vérifie la longueur du mot de passe et sa compléxité
			elseif ( preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).{8,}$#', $_POST['password']) == 0 ) {
				// La fonctionpreg_match() permet de rechercher une ou plusieurs occurrences d'un caractère.
				// On l'utilise ici pour tester la présence de nos différentes familles dans la chaîne entrée pour le mot de passe.
				// Le symbole# délimite le regex.
				// Le symbole^ placé au début indique tout simplement le début de la chaine.
				// (?=.*[a-z]) permet de tester la présence de minuscules.
				// (?=.*[A-Z]) permet de tester la présence de majuscules.
				// (?=.*[0-9]) permet de tester la présence de chiffres.
				// (?=.*\W) permet de tester la présence de caractères spéciaux (\W indique ce qui ne correspond pas à un mot).
  		 		 ?>
				<form class='error'>
					<p>Le champ mot de passe ne correspond pas à notre politique de sécurité, veuillez entrer au moins 8 carractères, des minuscules, des majuscules, des chiffres et des caractères spéciaux.</p>
					<button class="btn bnt-outline-secondary"><a href="formulaire.php">Retour</a></button>
				</form>
				<?php
				exit;
			}
			// On vérifie maintenant si le champ "email" n'est pas vide et existe bien
			elseif(empty($_POST['email'])) {
				?>
				<form class='error'>
					<p>Le champ email est vide.</p>
					<button class="btn bnt-outline-secondary"><a href="formulaire.php">Retour</a></button>
				</form>
				<?php
				exit;
			}
			// Tout les champs existes et ne sont pas vide, on continu
			else {
				// Les champs sont bien posté et pas vide, on sécurise les données entrées par le membre:
				// Le htmlentities() passera les guillemets en entités HTML, ce qui empêchera les injections SQL
				$login = htmlentities(strtolower ( $_POST['login'] ), ENT_QUOTES, "ISO-8859-1");
				$nom = htmlentities($_POST['nom'], ENT_QUOTES, "ISO-8859-1");
				$prenom = htmlentities($_POST['prenom'], ENT_QUOTES, "ISO-8859-1");
				$password = htmlentities($_POST['password'], ENT_QUOTES, "ISO-8859-1");
				$email = htmlentities($_POST['email'], ENT_QUOTES, "ISO-8859-1");

				// On se connecte à la base de données
				$connexion = new PDO('mysql:host='.$lehost.';port='.$leport.';dbname='.$dbname, $user, $pass);

				// On envoie la requête pour vérifier si le login existe déjà ou non
				$resultats=$connexion->query("SELECT login FROM utilisateurs WHERE login='".$login."'");
				$resultats->setFetchMode(PDO::FETCH_OBJ);
				while( $resultat = $resultats->fetch() ){
					$compt++;
				}

				// Fermeture de la connexion à la base de données
				$resultats->closeCursor();

				// Test sur la réussite de la reqête
				if ($compt == 0) {
					// On se connecte à la base de données
					$connexion = new PDO('mysql:host='.$lehost.';port='.$leport.';dbname='.$dbname, $user, $pass);

					// On envoie la requête pour remplir la base
					$req = $connexion->prepare('INSERT INTO utilisateurs(nom, prenom, email, login, password) VALUES(:nom, :prenom, :email, :login, :password)');
					$req->execute(array(
						'nom' => $nom,
						'prenom' => $prenom,
						'email' => $email,
						'login' => $login,
						'password' => password_hash($password, PASSWORD_DEFAULT)
						));

					?>
					<form class='success'>
						<p>L'utilisateur a bien été ajouté !</p>
						<button class="btn bnt-outline-secondary"><a href="index.php">Retour</a></button>
					</form>
					<?php
				}
				// Si le compteur n'est pas égale à 0 alors il y a déja le même login
				else {
					?>
					<form class='error'>
						<p>L'identifiant entré est déjà utilisé.</p>
						<button class="btn bnt-outline-secondary"><a href="formulaire.php">Retour</a></button>
					</form>
					<?php
					exit;
				}
			}
		?>

	</body>
</html>
