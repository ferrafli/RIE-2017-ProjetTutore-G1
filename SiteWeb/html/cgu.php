<?php
session_start(); // Demmare la Session

// Afficher les erreurs à l'écran
// ini_set('display_errors', 1);

/*##################################################
#	      cgu.php                                             #
#       Ver 1.0 - Date 18-12-2018                           #
#       Page de Conditions Générales d'Utilisation du site  #
##################################################*/
?>
<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <meta http-equiv="Content-Security-Policy" content="default-src https:"> -->
   	<link rel="shortcut icon" type="image/x-icon" href="/Imgs/favicon.ico" />

    <title>Conditions Générales d'Utilisations</title>

    <!-- Bootstrap core CSS -->
    <link href="./style/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./style/css/sticky-footer-navbar.css" rel="stylesheet">
  </head>

  <?php include_once('header.php'); ?>
  
  <body>
  	<h1>Conditions générales d'utilisation du site www.trueack.ovh</h1>
  	<p>
      <h2>ARTICLE 1 : Objet</h2>
        <p>Les présentes « conditions générales d'utilisation » ont pour objet l'encadrement juridique des modalités de mise à disposition des services du site www.trueack.ovh et leur utilisation par « l'Utilisateur ».</p>
        <p>Les conditions générales d'utilisation doivent être prisent en compte par tout Utilisateur souhaitant s’inscrire sur notre site. Elles constituent le contrat entre le site et l'Utilisateur.</p>
        <p>L’accès avec authentification au site par l’Utilisateur signifie son acceptation des présentes conditions générales d’utilisation.</p>
        <p>www.trueack.ovh se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes conditions générales d'utilisation.</p>
        </p>
      <h2>ARTICLE 2 : Mentions légales</h2>
        <p>L'édition du site www.trueack.ovh est assurée par la Société OVH SAS au capital de 10.069.020 € dont le siège social est situé au 2 rue Kellermann - 59100 Roubaix – France.
        </p>
      <h2>ARTICLE 3 : Définitions</h2>
        La présente clause a pour objet de définir les différents termes essentiels du contrat :
        <ul>
          <li>Utilisateur : ce terme désigne toute personne qui utilise le site ou l'un des services proposés par le site.</li>
          <li>Contenu utilisateur : ce sont les données transmises par l'Utilisateur au sein du site.</li>
          <li>Membre : l'Utilisateur devient membre lorsqu'il est identifié sur le site.</li>
          <li>Identifiant et mot de passe : c'est l'ensemble des informations nécessaires à l'identification d'un Utilisateur sur le site. L'identifiant et le mot de passe permettent à l'Utilisateur d'accéder à des services réservés aux membres du site. Le mot de passe est confidentiel.</li>
        </ul>
      <h2>ARTICLE 4 : Accès aux services</h2>
        Le site permet à l'Utilisateur un accès gratuit aux services suivants :
        <ul>
          <li>Page accueil et de connexion;</li>
          <li>Page Contact;</li>
          <li>Page de CGU;</li>
          <li>Formulaire d’inscription;</li>
        </ul>
        <p>Le site est accessible gratuitement en tout lieu à tout Utilisateur ayant un accès à Internet. Tous les frais supportés par l'Utilisateur pour accéder au service (matériel informatique, logiciels, connexion Internet, etc.) sont à sa charge.
        L’Utilisateur non membre n'a pas accès aux services réservés aux membres. Pour cela, il doit s'identifier à l'aide de son identifiant et de son mot de passe.</p>
        <p>Le site met en œuvre tous les moyens mis à sa disposition pour assurer un accès de qualité à ses services. L'obligation étant de moyens, le site ne s'engage pas à atteindre ce résultat.</p>
        <p>Tout événement dû à un cas de force majeur ayant pour conséquence un dysfonctionnement du réseau ou du serveur n'engage pas la responsabilité de Trueack.</p>
        <p>L'accès aux services du site peut à tout moment faire l'objet d'une interruption, d'une suspension, d'une modification sans préavis pour une maintenance ou pour tout autre cas. L'Utilisateur s'oblige à ne réclamer aucune indemnisation suite à l'interruption, à la suspension ou à la modification du présent contrat.</p>
        L'Utilisateur a la possibilité de contacter le site par messagerie électronique à l’adresse message@trueack.ovh.
      <h2>ARTICLE 5 : Propriété intellectuelle</h2>
        <p>Les marques, logos, signes et tout autre contenu du site font l'objet d'une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d'auteur.</p>
        <p>Les contenus figurant sur le site sont la propriété de Trueack. Toute utilisation, reproduction ou représentation, par quelque procédé que ce soit, et sur quelque support que ce soit, de tout ou partie du site et/ou des éléments qui le composent n'est pas autorisée sans le consentement expresse de l’équipe Trueack.</p>
      <h2>ARTICLE 6 : Données personnelles</h2>
        <p>Les informations demandées à l’inscription au site sont nécessaires et obligatoires pour la création du compte de l'Utilisateur. En particulier, l'adresse électronique pourra être utilisée par le site pour l'administration, la gestion et l'animation du service.</p>
        <p>Le site assure à l'Utilisateur une collecte et un traitement d'informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.<p>
        En vertu des articles 39 et 40 de la loi en date du 6 janvier 1978, l'Utilisateur dispose d'un droit d'accès, de rectification, de suppression et d'opposition de ses données personnelles. L'Utilisateur exerce ce droit via :
        • par mail à message@trueack.ovh;
      <h2>ARTICLE 7 : Responsabilité et force majeure</h2>
        <p>Les sources des informations diffusées sur le site sont réputées fiables. Toutefois, le site se réserve la faculté d'une non-garantie de la fiabilité des sources. Les informations données sur le site le sont à titre purement informatif. Ainsi, l'Utilisateur assume seul l'entière responsabilité de l'utilisation des informations et contenus du présent site.
        </p>
        <p>L'Utilisateur s'assure de garder son mot de passe secret. Toute divulgation du mot de passe, quelle que soit sa forme, est interdite.</p>
        <p>L'Utilisateur assume les risques liés à l'utilisation de son identifiant et mot de passe. Le site décline toute responsabilité.</p>
        <p>Tout usage du service par l'Utilisateur ayant directement ou indirectement pour conséquence des dommages doit faire l'objet d'une indemnisation au profit du site.</p>
        <p>Une garantie optimale de la sécurité et de la confidentialité des données transmises n'est pas assurée par le site. Toutefois, le site s'engage à mettre en œuvre tous les moyens nécessaires afin de garantir au mieux la sécurité et la confidentialité des données.</p>
        La responsabilité du site ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d'un tiers.
      <h2>ARTICLE 8 : Liens hypertextes</h2>
        <p>De nombreux liens hypertextes sortants sont présents sur le site, cependant les pages web où mènent ces liens n'engagent en rien la responsabilité de Trueack qui n'a pas le contrôle de ces liens.</p>
        <p>L'Utilisateur s'interdit donc à engager la responsabilité du site concernant le contenu et les ressources relatives à ces liens hypertextes sortants.</p>
      <h2>ARTICLE 9 : Évolution du contrat</h2>
        Le site se réserve à tout moment le droit de modifier les clauses stipulées dans le présent contrat.
      <h2>ARTICLE 10 : Durée</h2>
        La durée du présent contrat est indéterminée. Le contrat produit ses effets à l'égard de l'Utilisateur à compter de l'utilisation du service.
      <h2>ARTICLE 11 : Droit applicable et juridiction compétente</h2>
        La législation française s'applique au présent contrat. En cas d'absence de résolution amiable d'un litige né entre les parties, seuls les tribunaux sont compétents.

    </p>
    <p>
      Pour toutes demandes concernant la protection des données ou demande au sujet des propriétés intellectuelles du site:
  	   <a href="mailto:message@trueack.ovh">message@trueack.ovh</a>
    <p>
  </body>

  <?php include_once('footer.php'); ?>
 </html>