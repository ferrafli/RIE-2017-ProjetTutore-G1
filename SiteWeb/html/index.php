<?php
session_start(); // Demmare la Session

// Afficher les erreurs à l'écran
// ini_set('display_errors', 1);

/*##################################################
#	      ixndex.php                                 #
#       Ver 2.0 - Date 14-12-2018                  #
#       Page de connexion du site                  #
##################################################*/
?>
<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <meta http-equiv="Content-Security-Policy" content="default-src https:"> -->
    <link rel="shortcut icon" type="image/x-icon" href="/Imgs/favicon.ico" />

    <title>Trueack</title>

    <!-- Bootstrap core CSS -->
    <link href="./style/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./style/css/sticky-footer-navbar.css" rel="stylesheet">
  </head>

  <body>

    <?php

      // On défini une valeur de base pour le topic
      $topic = "topic";

      // Si on a un topic en GET on va remplacer la valeur par défaut
      if( isset($_GET['topic']) ) {
              $topic = $_GET['topic'];
      }

      // Nous forçons la suppression des sessions avant de commencer
      if(!empty($_SESSION['login'])){
        require("deconnexion.php");
      }

      include_once('header.php'); ?>

    <!-- Begin page content -->
    <main role="main" class="container">
      <div class="logo">
        <img src="./Imgs/Logo_TrueAck.png" width="50%" />
      </div>
      <h1><center>Connexion</center></h1>

      <div class="centrage">
        <form METHOD="post" ACTION="accueil.php">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Entrer identifiant" name="login" value="" required/>
          </div>

          <div class="form-group">
            <input type="password" class="form-control" placeholder="Entrer mot de passe" name="password" value="" required/>
          </div>

          <!-- Envoi en caché du nom de topic -->
          <input name="topic" type="hidden" value="<?php echo $topic; ?>">

          <div class="form-group">
            <input class="btn btn-outline-secondary" type="submit" value="Se Connecter">
          </div>
        </form>
      </div>
      <div class="centrage">
      <p>Bonjour. Souhaitez-vous vous inscrire <a href="formulaire.php">voici le formulaire</a> ?</p>
      </div>

    </main>

    <?php include('footer.php'); ?>

  </body>
</html>
