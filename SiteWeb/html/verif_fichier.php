<?php
session_start(); // Demmare la Session

// Afficher les erreurs à l'écran
// ini_set('display_errors', 1);

/*##################################################
#	      verif_fichier.php                          #
#       Ver 1.0 - Date 14-12-2018                  #
#       Page de vérification du fichier avec hash  #
##################################################*/
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/x-icon" href="/Imgs/favicon.ico" />

    <title>Vérification fichier</title>

    <!-- Bootstrap core CSS -->
    <link href="./style/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./style/css/sticky-footer-navbar.css" rel="stylesheet">
  </head>
	<body>
		<?php include_once('header.php');

		    // On fait le hash du fichier à télécharger
			$chemin = "/var/www/html/".$_POST['path'];
			$hash_verif = hash_file('sha256', $chemin);

            // Test des 2 hashs
            if (strcmp($_POST['hash'], $hash_verif) == 0) {
                ?>

						<form>
							<div class='success'>
								<p>Vous avez bien le bon fichier !</p>
							</div>

							<div>
								<button class="btn bnt-outline-secondary"><a href="accueil.php">Retour</a></button>
							</div>
							<button><a href="<?php echo $_POST['path']; ?>" download>Télécharger</a></button>
						</form>

                <?php
            		} else {
                ?>
					<div class='error'>
						<form>
							<p>Votre fichier n'est pas l'original !</p>
							<input type="button" value="Retour" onclick="history.go(-1)">
						</form>
					</div>
                <?php
            		}
			?>
      <?php include_once('footer.php'); ?>
	</body>
</html>
