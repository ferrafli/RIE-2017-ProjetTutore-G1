<?php
session_start(); //Demmarre la session

/*##################################################
#	    moncompte.php                              #
#       Ver 1.0 - Date 14-12-2018                  #
#       Page de gestion du compte                  #
##################################################*/
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <!-- <meta http-equiv="Content-Security-Policy" content="default-src https:"> -->
	    <link rel="shortcut icon" type="image/x-icon" href="/Imgs/favicon.ico" />

	    <title>Trueack</title>

	    <!-- Bootstrap core CSS -->
	    <link href="./style/css/bootstrap.css" rel="stylesheet">

	    <!-- Custom styles for this template -->
	    <link href="./style/css/sticky-footer-navbar.css" rel="stylesheet">
  
		<title>Mon Compte</title>
	</head>
	<body>
		<?php include_once('header.php'); ?>

		<section id="section">
			<div>
		
				<h1>Informations de votre compte</h1>
					<p>Votre Prenom:
					<?php
						echo $_SESSION['prenom'];
					?>
					</p>

					<p>Votre Nom:
					<?php
						echo $_SESSION['nom'];
					?>
					</p>

					<p>Votre Login:
					<?php
						echo $_SESSION['login'];
					?>
					</p>

					<p>Votre Email:
					<?php
						echo $_SESSION['email'];
					?>
					</p>
					
			
			</div>
		</section>

	</body>
	<?php include_once('footer.php'); ?>
</html>