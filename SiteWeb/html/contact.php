<?php
session_start(); // Demmare la Session

// Afficher les erreurs à l'écran
// ini_set('display_errors', 1);

/*##################################################
#	      Contact.php                                #
#       Ver 2.0 - Date 14-12-2018                  #
#       Page de contact du site                    #
##################################################*/
?>
<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <meta http-equiv="Content-Security-Policy" content="default-src https:"> -->
   	<link rel="shortcut icon" type="image/x-icon" href="/Imgs/favicon.ico" />

    <title>Contact</title>

    <!-- Bootstrap core CSS -->
    <link href="./style/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./style/css/sticky-footer-navbar.css" rel="stylesheet">
  </head>

  <?php include_once('header.php'); ?>
  
  <body>
  	<h1>Nous contacter</h1>
  	<p>Si vous rencontrez un problème, ou pour toute autre demande: n'hésitez pas à nous écrire à l'adresse suivante</p>
  	<a href="mailto:message@trueack.ovh">message@trueack.ovh</a>
  </body>

  <?php include_once('footer.php'); ?>
 </html>