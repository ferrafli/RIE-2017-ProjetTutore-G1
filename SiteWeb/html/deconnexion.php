<?php
// Declare la session
session_start ();

// On écrasse le tableau de session
$_SESSION = array ();


// Destruction des variables de notre session
session_unset ();

// Desruction de la session
session_destroy ();
unset ($_SESSION);
unset ($_COOKIE);

// Suppression des cookies de connexion automatique

//$value = "0";

setcookie ("login", "", time () - 5);
setcookie ("password", "", time () - 5);
//setcookie ('password', $value, time () + 5);

// Redirection du visiteur vers la page d'accueil

header ("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP 1.1
header ("Pragma: no-cache"); // HTTP 1.0
header ("Expires: 0"); // Proxies

header ('location: ./index.php');

?>
