<?php
/*##################################################
#	    certif-lu.php                              #
#       Ver 1.1 - Date 14-12-2018                  #
#       Liaison avec certifiant                    #
##################################################*/

// Notification à l'utilisateur si le serveur termine la connexion
function my_ssh_disconnect($reason, $message, $language) {
    printf("Serveur deconnecte avec l'erreur code [%d] et le message: %s\n",
         $reason, $message);
}
$callbacks = array('disconnect' => 'my_ssh_disconnect');

$connection = ssh2_connect('164.132.111.226', 52677, array('hostkey'=>'ssh-rsa'), $callbacks); //ssh-dss -> ssh-rsa
if (!$connection) die('Échec de la connexion');
//echo 'connexion OK<br>';

$fingerprint = '3CA4C19EEDBAD35C618967363767D020';
$quicest = ssh2_fingerprint($connection, SSH2_FINGERPRINT_MD5 | SSH2_FINGERPRINT_HEX);
//echo $quicest;

if (ssh2_fingerprint($connection, SSH2_FINGERPRINT_MD5 | SSH2_FINGERPRINT_HEX) != $fingerprint) 
    die ('<br>problème d\'identification du serveur: la signature reçue ne correspond pas à son empreinte enregistree!') ;
//echo '<br>Connexion en cours<br>';  



if (ssh2_auth_pubkey_file($connection, 'MasterRIE','/var/www/.ssh/id_rsa.pub','/var/www/.ssh/id_rsa')) // $passphrase = null
    {
    //echo 'Connexion reussie<br>' ;
    //echo 'Authentification par cle publique reussie<br>';
    }
    else    {
        die('<br>'."Erreur lors de l'authentification par cle publique"); 
    }

//echo 'début copie de fichier<br>';
if(ssh2_scp_recv($connection, 'signer_env.php', '/var/www/.ssh/signer.php')){
    //echo 'Copie réussie<br>';
}
else{
    echo '<br>'."Erreur de copie".'<br>';
}
//echo 'fin copie de fichier<br>';

// execution de la signature
require_once('/var/www/.ssh/signer.php');
//echo '<br>Message certifiant inscrit<br>';

// remise a "zero" du fichier signer.php
//echo 'début copie de fichier<br>';
if(ssh2_scp_recv($connection, 'signer_vide.php', '/var/www/.ssh/signer.php')){
   // echo 'Copie réussie<br>';
}
else{
    echo '<br>'."Erreur de copie".'<br>';
}
//echo 'fin copie de fichier<br>';

//Sauvegarde de certification


$commande = './kafka_2.10-0.10.2.1/bin/kafka-simple-consumer-shell.sh --broker-list trueack.ovh:9092 --offset -2 --max-messages 11 --partition 0 --topic '.$nom_topic.' > ./Sauvegarde/'.$nom_topic.'.txt';

//exécution command shell sur la machine destinataire
if (!$stream = ssh2_exec($connection, $commande)) {
    echo '<br>'."Erreur d'exécution commande shell";
}

?>
