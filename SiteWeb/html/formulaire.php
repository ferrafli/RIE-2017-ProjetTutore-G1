<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		
		<link rel="shortcut icon" type="image/x-icon" href="/Imgs/favicon.ico" />
		<title>Trueack - Inscripion</title>

		<!-- Bootstrap core CSS -->
		<link href="./style/css/bootstrap.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="./style/css/sticky-footer-navbar.css" rel="stylesheet">
	</head>

	<body>
		<?php include_once('header.php'); ?>
		<p><img src="./Imgs/Logo_TrueAck.png" width="20%" /></p>

		<h1>Inscription</h1>

		<form METHOD="post" ACTION="inscription.php"  >
    		<p>Identifiant de connexion :</p>
    		<p>10 caractères maximum</p>
    		<div class="form-group">
            	<input type="text" class="form-control" name="login" value="" maxlength="10" required/>
          	</div>

    		<p>Prénom :</p>
			<div class="form-group">
            	<input type="text" class="form-control" name="prenom" value="" required/>
          	</div>

			<p>Nom : </p>
			<div class="form-group">
            	<input type="text" class="form-control" name="nom" value="" required/>
          	</div>

			<p>Mot de passe : </p>
			<div class="form-group">
            	<input type="password" class="form-control" name="password" value="" pattern=".{8,}" required/>
          	</div>
			<p>Le mot de passe doit contenir : 8 caractères, des minuscules, des majuscules, des chiffres et des caractères spéciaux.</p>

			<p>Email : </p>
			<div class="form-group">
            	<input type="email" class="form-control" name="email" value="" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" required/>
          	</div>
          	
          	<div class="form-group">
				<p>
					<button class="btn bnt-outline-secondary"><a href="index.php">Retour</a></button>
					<input class="btn btn-outline-secondary" type="submit" name="inscription" value="Inscription">
				</p>
			</div>
    	</form>

		<?php include_once('footer.php'); ?>
    </body>
</html>
