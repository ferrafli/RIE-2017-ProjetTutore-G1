
<?php
// Gestion des sessions
session_start();

// Afficher les erreurs à l'écran
// ini_set('display_errors', 1);

/*##################################################
#	    Consommateur.php                           #
#       Ver 2.0 - Date 14-12-2018                  #
#       Page de lecture du message                 #
##################################################*/
?>

<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" type="image/x-icon" href="/Imgs/favicon.ico"/>

		<title>Accueil</title>

		<!-- Bootstrap core CSS -->
		<link href="./style/css/bootstrap.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="./style/css/sticky-footer-navbar.css" rel="stylesheet">


		<script type="text/javascript">
		
			function bascule(id) { 
				if (document.getElementById(id).style.visibility == "hidden")
						document.getElementById(id).style.visibility = "visible"; 
				else	document.getElementById(id).style.visibility = "hidden"; 
			} 

		</script> 

	</head>
	<body >


<h1>Lecture de votre recommandé éléctronique.<br></h1>
<?php 
	include('header.php');

	// Déclaration d'une variable pour vérifier la bonne lecture du topic, et donc si on bien le bon topic ou pas.
	$Lecture_topic = 0;
	
	// Elements de connexion à la BDD
	require_once("paramcon.php");

	// On sécurise les données entrées par le membre.
	// Le htmlentities() passera les guillemets en entités HTML, ce qui empêchera les injections SQL.
	$topic = htmlentities($_POST['topic'], ENT_QUOTES, "ISO-8859-1");
	$pass_topic = htmlentities($_POST['pass_topic'], ENT_QUOTES, "ISO-8859-1");

	// On se connecte à la base de données
	$connexion = new PDO('mysql:host='.$lehost.';port='.$leport.';dbname='.$dbname, $user, $pass);

	// On envoie la requête
	$resultats=$connexion->query("SELECT topics,password FROM topics WHERE topics='".$topic."'");
	$resultats->setFetchMode(PDO::FETCH_OBJ);
	while( $resultat = $resultats->fetch() ){
		$compt++;
		$pass_bdd = $resultat->password;
	}	

	// Fermeture de la connexion à la base de données
	$resultats->closeCursor();

	// Test sur la réussite de la reqête
	if ( $compt == 0 ) {
		?>
		<form class="error" action="accueil.php">
			<p>Erreur sur le nom de topic entré.</p>
			<input class="btn bnt-outline-secondary" type="submit" value="Retour">
		</form> 
		<?php
		exit;
	}

	// Vérification du mot de passe hashé
	if (!password_verify($pass_topic, $pass_bdd)) {
	//if ( $pass_topic != $pass_bdd ) {
		?>    
		<form class="error" action="accueil.php">
				<p>Erreur sur le mot de passe entré.</p>
				<input class="btn bnt-outline-secondary" type="submit" value="Retour">
		</form> 
		<?php   
		exit;   
	}
	
	// Producteur 
	$rk_p = new RdKafka\Producer();
	$rk_p->setLogLevel(LOG_DEBUG);
	$rk_p->addBrokers("127.0.0.1");
	$topic_p = $rk_p->newTopic($_POST['topic']);

	// Consommateur
	$rk = new RdKafka\Consumer();
	$rk->setLogLevel(LOG_DEBUG);
	$rk->addBrokers("127.0.0.1");
	$topic = $rk->newTopic($_POST['topic']);

	// The first argument is the partition to consume from.
	// The second argument is the offset at which to start consumption. Valid values
	// are: RD_KAFKA_OFFSET_BEGINNING, RD_KAFKA_OFFSET_END, RD_KAFKA_OFFSET_STORED.
	$topic->consumeStart(0, RD_KAFKA_OFFSET_BEGINNING);

	while (true) {
		// The first argument is the partition (again).
		// The second argument is the timeout.
		$msg = $topic->consume(0, 1000);

		// Si nous avons une erreur, alors nous sommes au bout du topic
		if ($msg->err) {
			// Envoi des messages dans Kafka
			// echo "Ajout de DATA_LUE";
			$topic_p->produce(RD_KAFKA_PARTITION_UA, 0, "#DATA_LUE#");
			// Ajout des champs pour le certifiant
			require_once("certif-lu.php");
			// Envoi du mail d'information d'un nouvel échange
			require_once("mail_conso.php");
			// On sort de la boucle
			break;
		}
			elseif ($msg->payload == '#DATA_INVISIBLES#') {
				$date = $topic->consume(0, 1000);
				// echo $date->payload."<br>";
				$nom = $topic->consume(0, 1000);
				// echo $nom->payload."<br>";
				$prenom = $topic->consume(0, 1000);
				// echo $prenom->payload."<br>";
				$email_envoi = $topic->consume(0, 1000);
				// echo $email_envoi->payload."<br>";
				$email_reception = $topic->consume(0, 1000);
				// echo $email_reception->payload."<br>";
				$fichier = $topic->consume(0, 1000);
				// echo $fichier->payload."<br>";
			}
			elseif ($msg->payload == '#DATA_VISIBLES#') {
				$hash = $topic->consume(0, 1000);
				$COM = $topic->consume(0, 1000);
				$Lecture_topic = 1;
					?>
					<div>
						<p>HASH de votre fichier : <?php echo $hash->payload; ?></p>
						<p>Commentaire associé à votre fichier : <?php echo $COM->payload; ?></p>
						<form action="verif_fichier.php" method="post" enctype="multipart/form-data">
							<!-- Envoi en caché de la valeur du hash contenu dans le topic -->
							<input name="hash" type="hidden" value="<?php echo $hash->payload; ?>">
							<!-- Envoi en caché de la valeur du PATH du fichier -->
							<input name="path" type="hidden" value="/Upload/<?php echo $_POST['topic'].'/'.$fichier->payload; ?>">
							<p><input type="submit" value="Accéder au fichier"></p>
						</form>
					</div>
					<?php
			}
			// Si nous avons déjà la balise #DATA_LUE# nous sortons
			elseif ($msg->payload == '#DATA_LUE#') {
				// echo "DATA_LUE";
				break;
			}
		}

		// Gestion de l'erreur de lecture du topic
		if ( $Lecture_topic == 0 ) {
			?>
			<form class='error'>
				<p>Le message demmandé n'est pas disponible.</p>
				<button class="btn bnt-outline-secondary"><a href="index.php">Retour</a></button>
				<!-- <input type="button" value="Retour" onclick="history.go(-1)"> -->
			</form>
			<?php
		}
	

	include('footer.php'); 
	?>

	</body>
</html>

