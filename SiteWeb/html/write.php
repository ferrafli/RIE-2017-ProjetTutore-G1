<?php
// // Gestion des sessions
session_start(); // Demmare la Session
if (!isset($_SESSION['login'])) {
  header('Location:index.php');
}

// Afficher les erreurs à l'écran
// ini_set('display_errors', 1);

/*##################################################
#	    write.php                                  #
#       Ver 2.0 - Date 14-12-2018                  #
#       Page d'écriture du topic                   #
##################################################*/
?>
<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" type="image/x-icon" href="/Imgs/favicon.ico" />

		<title>Ecrire un message</title>

		<!-- Bootstrap core CSS -->
		<link href="./style/css/bootstrap.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="./style/css/sticky-footer-navbar.css" rel="stylesheet">
	</head>

	<body>
    <?php include_once('header.php'); ?>
		<?php

			// Lecture des nouvelles valeurs dans le tableau de session
			$login = $_SESSION['login'];

				?>
				<h1>Vous êtes bien connecté <?php echo $login; ?> !</h1>
				<br>
				<h2>Nouvel échange.</h2>
				<div>
					<form action="producteur.php" method="post" enctype="multipart/form-data">
						<p>Document autorisé de type: png, gif, jpg, jpeg, txt ou doc...</p>
						<p>Choix du fichier :	<input type="file" name="fileToUpload" id="fileToUpload"></p>
						<p>Commentaire : <input type="textarea" name="COM" placeholder="Commentaire" /></p>
						<p>Adresse mail de votre correspondant : <input type="email" name="MAIL" placeholder="mail@trueack.ovh" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$"/></p>
						<p><input type="submit" value="Valider"></p>
					</form>
				</div>
				
				<div>
					<button class="btn bnt-outline-secondary"><a href="accueil.php">Retour</a></button>
				</div>

            <?php include_once('footer.php'); ?>


		</body>
</html>
