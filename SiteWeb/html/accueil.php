<?php
// Gestion des sessions
session_start(); 

// Afficher les erreurs à l'écran
// ini_set('display_errors', 1);

/*##################################################
#	    accueil.php                                #
#       Ver 2.0 - Date 14-12-2018                  #
#       Page accueil du site	                   #
##################################################*/
?>
<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" type="image/x-icon" href="/Imgs/favicon.ico"/>

		<title>Accueil</title>

		<!-- Bootstrap core CSS -->
		<link href="./style/css/bootstrap.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="./style/css/sticky-footer-navbar.css" rel="stylesheet">

		<script type="text/javascript">
		
			function bascule(id) { 
				if (document.getElementById(id).style.visibility == "hidden")
						document.getElementById(id).style.visibility = "visible"; 
				else	document.getElementById(id).style.visibility = "hidden"; 
			} 

		</script> 
	</head>
	<body >

		<?php
			//Appel au header du site
			include('header.php');

			// Elements de connexion à la BDD
			require_once("paramcon.php");

			// Déclaration du compteur pour déterminer la réussite de nos requêtes SQL
			$compt=0;

			// Si nous avons déjà un session, nous ne faisons pas les contrôles
			if ( !empty($_SESSION['login']) ) {
				?>
				<h1>Vous êtes bien connecté <?php echo $_SESSION['login']; ?> !</h1>
				<br>
				<form action="write.php">
					<button class="btn btn-outline-secondary" type="submit">Écrire un message</button>
				</form>
				<br>

				<button class="btn btn-outline-secondary" onclick="bascule('form');">Lire un message</button>

				<form METHOD="post" ACTION="consommateur.php" id="form" style="visibility: hidden;">
		    			<p>Nom de topic</p>
    					<div class="form-group">
		        		    	<input type="text" class="form-control" name="topic" value="" required/>
          				</div>

					<p>Mot de passe : </p>
					<div class="form-group">
			        	    	<input type="password" class="form-control" name="pass_topic" value="" pattern=".{6,}" required/>
          				</div>
          	
          				<div class="form-group">
						<input class="btn btn-outline-secondary" type="submit" name="Valider" value="Valider">
					</div>
			    	</form>

				<?php

    			include('footer.php'); 
				exit;
			}

			// Phase de tests des champs entré par l'utilisateur
			if( empty($_POST['login']) ) {
			// on vérifie que le champ "Login" n'est pas vide
			// empty vérifie à la fois si le champ est vide et si le champ existe belle et bien (is set)
				?>
				<form class="error" action="deconnexion.php">
					<p>Le champ login est vide.</p>
					<input class="btn bnt-outline-secondary" type="submit" value="Retour">
				</form> 
				<?php
				exit;
			}
			// On vérifie maintenant si le champ "Password" n'est pas vide
			elseif( empty($_POST['password']) ) {
				?>
				<form class="error" action="deconnexion.php">
					<p>Le champ mot de passe est vide.</p>
					<input class="btn bnt-outline-secondary" type="submit" value="Retour">
				</form> 
				<?php
				exit;
			}
			// Les champs sont bien posté et pas vide, on poursuit
			else {
				// On sécurise les données entrées par le membre.
				// Le htmlentities() passera les guillemets en entités HTML, ce qui empêchera les injections SQL.
				// Le strtolower va transformer un str en minuscule.
				$login = htmlentities(strtolower ( $_POST['login'] ), ENT_QUOTES, "ISO-8859-1");
				$password = htmlentities($_POST['password'], ENT_QUOTES, "ISO-8859-1");

				// On se connecte à la base de données
				$connexion = new PDO('mysql:host='.$lehost.';port='.$leport.';dbname='.$dbname, $user, $pass);

				// On envoie la requête
				$resultats=$connexion->query("SELECT login,password,email,nom,prenom FROM utilisateurs WHERE login='".$login."'");
				$resultats->setFetchMode(PDO::FETCH_OBJ);
				while( $resultat = $resultats->fetch() ){
					$compt++;
					// Ecriture des nouvelles valeurs dans le tableau de session
					$_SESSION['login'] = $login;
					$_SESSION['email'] = $resultat->email;
					$_SESSION['nom'] = $resultat->nom;
					$_SESSION['prenom'] = $resultat->prenom;
					$_SESSION['password'] = $resultat->password;
				}

				// Fermeture de la connexion à la base de données
				$resultats->closeCursor();

				// Test sur la réussite de la reqête
				if ( $compt == 0 ) {
					?>
					<form class="error" action="deconnexion.php">
						<p>Erreur sur l'identifiant.</p>
						<input class="btn bnt-outline-secondary" type="submit" value="Retour">
					</form> 
					<?php
					exit;
				}

				// Vérification du mot de passe hashé
				if (password_verify($password, $_SESSION['password'])) {
					// Tout est OK, nous affichons la page voulue
					if ($_POST['topic'] == "topic") {
						//Appel au header du site
						include('header.php');
						?>
						<h1>Vous êtes bien connecté <?php echo $login; ?> !</h1>
						<br>
						<form action="write.php">
							<button class="btn btn-outline-secondary" type="submit">Écrire un message</button>
						</form>
						<br>
						<button class="btn btn-outline-secondary" onclick="bascule('form');">Lire un message</button>

						<form METHOD="post" ACTION="consommateur.php" id="form" style="visibility: hidden;">
							<p>Nom de topic</p>
							<div class="form-group">
								<input type="text" class="form-control" name="topic" value="" required/>
							</div>

							<p>Mot de passe : </p>
							<div class="form-group">
								<input type="password" class="form-control" name="pass_topic" value="" pattern=".{6,}" required/>
							</div>

							<div class="form-group">
								<input class="btn btn-outline-secondary" type="submit" name="Valider" value="Valider">
							</div>
						</form>
						<?php
					} else {
						$_SESSION['login'] = $login;
						?>
						<form METHOD="post" ACTION="consommateur.php" >
							<!-- Envoi en caché du nom de topic -->
							<input name="topic" type="hidden" value="<?php echo $_POST['topic']; ?>">
							<p>Mot de passe : </p>
							<div class="form-group">
								<input type="password" class="form-control" name="pass_topic" value="" pattern=".{6,}" required/>
							</div>

							<div class="form-group">
								<input class="btn btn-outline-secondary" type="submit" name="Valider" value="Valider">
							</div>
						</form>
						<?php
					}

				} else {
					?>
					<form class="error" action="deconnexion.php">
						<p>Erreur sur le mot de passe.</p>
						<input class="btn bnt-outline-secondary" type="submit" value="Retour">
					</form> 
					<?php
					exit;
				}
			}
			
    		include('footer.php'); 
		?>
		</body>
</html>

