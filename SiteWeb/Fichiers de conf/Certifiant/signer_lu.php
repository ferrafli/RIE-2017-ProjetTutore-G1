<?php
/*##################################################
#	    signer_lu.php                              #
#       Ver 1.1 - Date 09-12-2018                  #
#       Fichier sur le certifiant                  #
##################################################*/

// Envoi des messages dans Kafka
// Balise DATA non visible par l'utilisateur
$preuve_lu = "#CERTIFICATION DE LECTURE#".time();
$topic_p->produce(RD_KAFKA_PARTITION_UA, 0, $preuve_lu);

?>
