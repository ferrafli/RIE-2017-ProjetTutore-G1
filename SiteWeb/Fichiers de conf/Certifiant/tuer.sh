#!/bin/bash

####################################################
#	    signer_env.php                             #
#       Ver 1.1 - Date 19-12-2018                  #
#       Fichier sur le certifiant                  #
####################################################

#Liste les PID des processus "java" puis les kill en //
#Petit probème de fonctionnement
#pidof pname "java" | tr ' ' '\n' | { while read line; do kill $line; done; }

#permet de tuer le processus java -> kafka (car utilise java)
#n'est plus utilisé car l'accès à kafka se fait par nombre de message fixe ET connu
kill `ps -aux | grep "java"`