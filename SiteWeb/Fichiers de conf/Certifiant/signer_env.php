<?php
/*##################################################
#	    signer_env.php                             #
#       Ver 1.1 - Date 14-12-2018                  #
#       Fichier sur le certifiant                  #
##################################################*/


// Envoi des messages dans Kafka
// Balise DATA non visible par l'utilisateur
$preuve_env = "CERTIFICATION D'ENVOI#".time();
$topic->produce(RD_KAFKA_PARTITION_UA, 0, $preuve_env);

?>
