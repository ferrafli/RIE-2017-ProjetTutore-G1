#!/bin/bash

####################################################
#	    cleanUpload.sh                             #
#       Ver 1.0 - Date 14-12-2018                  #
#       suppression fichiers de +7 jours           #
#       le script se lance par crontab depuis home #
####################################################

# marche si repertoire vide
#find /var/www/html/Upload/ -type d -mtime +7 -delete;

#marche avec repertoire non vide
find /var/www/html/Upload/ -type d -mtime +7 -exec rm -rv {} + ;
